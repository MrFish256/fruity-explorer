#!/usr/bin/env python
"""
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

Copyright 2014 Jeffrey Jansen
"""
from scapy.all import *
import getopt
import md5
import sys
import collections
import os
import string
from operator import itemgetter
import subprocess, threading
from multiprocessing import Process
from prettytable import *
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
logger = logging.getLogger('fruity')
logger.info("[Starting Fruity-Explorer]")

# SSID which is used to probe and identify a karma device. This SSID should 
# NOT exist in your neigbourhoud. Max 32 chars. Use the word "random" somewhere the SSID
# to randomize the chars.
ssid = "FRUITYeXplorer123"

# Interface which we will use for sniffing
interface = "mon0"

# Source MAC address used for pobing
sourceMac = "D7:27:6D:04:10:62"

# Logfile location
logfile = "/tmp/fruity.log"

# Loglevel: DEBUG, INFO, WARN, ERROR, CRITICAL
loglevel = logging.WARN

########## ADVANCED ###########

# Max. age in seconds when the AP should be removed if it isn't 
# seen anymore.
autoRemoveAge = 50 

# The interval of the autoRemoveTimer. Should always be lower than 
# autoRemoveAge. 5 seconds should be good.
autoRemoveTimerInterval = 10

# How long fruity explorer must wait before frequency will be changed. 
# Depending on the network card and latency this should be around
# 0.5 seconds. Lower is a faster frequency change.
freqChangeSleep = 0.5

if not 'SUDO_UID' in os.environ.keys():
    print "Run as root please"
    sys.exit(1)

class Event:
    def __init__(self):
        self.handlers = set()

    def handle(self, handler):
        self.handlers.add(handler)
        return self

    def unhandle(self, handler):
        try:
            self.handlers.remove(handler)
        except:
            raise ValueError("Handler is not handling this event, so cannot unhandle it.")
        return self

    def fire(self, *args, **kargs):
        for handler in self.handlers:
            handler(*args, **kargs)

    def getHandlerCount(self):
        return len(self.handlers)

    __iadd__ = handle
    __isub__ = unhandle
    __call__ = fire
    __len__  = getHandlerCount

class AccessPointStatus:
    CERTAIN = "\033[91mKARMA\033[95m" 
    TENTATIVE = "\033[93mKARMA\033[95m"
    FIRM = "\033[94mKARMA\033[95m"
    NORMAL = "\033[92mOK\033[95m"

# AccessPoint class
class AccessPoint:
      def __init__(self, packet=None, status=AccessPointStatus.NORMAL):
         self.packet = packet
         self.status = status
         self.alive = time.time()
         try:
            self.rssi = int(-(256-ord(self.packet.notdecoded[-4:-3]))) 
         except:
            self.rssi = -1
            logger.error("Sorry, was not able to retrieve RSSI from Dot11 packet. Your netwerk adapter doesnt support this probably.")
            pass

      def __getitem__(self, item):
         return self.getSsid()
      
      def setStatus(self, status):
          self.status = status

      def setRSSI(self, rssi):
          self.rssi = rssi

      def setAlive(self):
          self.alive = time.time()

      def setAlive(self, alive):
          self.alive = alive

      def getChannel(self):
          return int(ord(self.packet[Dot11Elt:3].info))

      def calcAlive(self):
          elapsed = time.time() - self.alive
          hours, rest = divmod(elapsed, 3600)
          minutes, seconds = divmod(rest, 60)
          return seconds + (minutes * 60)

      def getRSSI(self):
          return self.rssi 

      def getStatus(self):
          return self.status

      def getAlive(self):
          return self.alive

      #@FIXME We should add more unique information for fingerprinting. Dont use the SSID here.
      def getHash(self):
         elt = self.packet[Dot11Elt]
         ssid = elt[0].info
         bsid = self.packet.addr2
         supprates = elt[1].info
         return str(md5.new(bsid + supprates).hexdigest())

      def getBsid(self):
        return self.packet.addr2
      
      def getSsid(self):
         ssid = self.packet[Dot11Elt].info
         #@FIXME We should filter all characters here, except the RFC allowed chars and printable ones.
         ssid = filter(lambda x: x in string.printable, ssid)
         if ssid == "":
            return "<hidden>"
         return ssid

class Prober:

    def __init__(self, ssid="FruityExplorer123", sourceMac="D7:27:6D:04:10:62"):
        self.ssid = ssid
        self.freqChangeSleep = 0.5
        self.fingerprint = None
        self.sourceMac = sourceMac
        self.onProbeSend = Event()
        self.onProbeReceived = Event()
        self.manager = None
        self.thread = None
    
    def setSsid(self, ssid):
        self.ssid = ssid[:32]
    
    def getSsid(self):
        #rand = ''.join(random.sample(string.ascii_lowercase, 5))
        #ssid = self.ssid.replace("random", rand)
        return self.ssid
    
    def setSourceMac(self, sourceMac):
        self.sourceMac = sourceMac

    def setFreqChangeSleep(self, sleep):
        self.freqChangeSleep = sleep

    def watch(self, modifiedList):
        for ap in modifiedList:
            if ap.getSsid() == self.getSsid() and ap.getStatus() == AccessPointStatus.NORMAL:
               self.manager.setKarma(ap)
               self.onProbeReceived(ap)
               
    def watchManager(self, manager):
        self.manager = manager
        manager.onListModified += self.watch

    def sendProbe(self):
        self.thread = Process(target=self.processProbe)
        logger.log(1, "Starting new thread to send our probe.")
        self.thread.start()

    def processProbe(self):
        try:
          while True:
              for channel in range(1, 14):
                 try:
                    command = "iw dev %s set channel %d"
                    logger.log(2, "Changing interface channel to {0}. Raw command: {1} ".format(channel, command))
                    os.system(command % (interface, channel))
                    time.sleep(self.freqChangeSleep)
                 except KeyboardInterrupt:
                    break

                 destination = "ff:ff:ff:ff:ff:ff"
                 pkt = RadioTap()/Dot11(type=0,subtype=4,addr1=destination, addr2=self.sourceMac, addr3=destination)
                 pkt /= Dot11Elt(ID=0,info=self.getSsid())
                 pkt /= Dot11Elt(ID=1,info="\x82\x84\x8b\x96\x0c\x12\x18$")
                 pkt /= Dot11Elt(ID='DSset',info=chr(channel))
                 logger.log(2, "Sending probe request now from {0} to {1} on channel {2} with SSID {3}".format(self.sourceMac, destination, channel, self.getSsid()))
                 sendp(pkt, iface=interface, verbose=0)
                 self.onProbeSend(pkt)
        except:
          logger.warn("Error during probe processing. Joining probe threat.")
          self.thread.join()

class AccessPointManager:
    def __init__(self, autoRemoveAge=15, autoRemoveTimerInterval=10):
        self.autoRemoveAge = autoRemoveAge
        self.autoRemoveTimerInterval = autoRemoveTimerInterval
        self.itemsHolder = []
        self.onListModified = Event()
        self.onAutoRemoveTimerFinished = Event()
        self.autoRemoveTimer = None 
        self.onAutoRemoveTimerFinished += self.startAutoRemoveTimer
        self.startAutoRemoveTimer()

    def setAutoRemoveAge(self, age):
        self.autoRemoveAge = age

    def setAutoRemoveTimerInterval(self, interval):
        self.autoRemoveTimerInterval = interval

    def add(self, ap):
        logger.debug("Adding accesspoint {0} to the manager".format(ap.getSsid()))
        self.itemsHolder.append(ap)
        self.onListModified(self.itemsHolder)

    def update(self, ap):
        findAp = self.findByHash(ap.getHash())
        if findAp != None and findAp.getSsid() == ap.getSsid():
           for i, iterationAp in enumerate(self.itemsHolder):
               if iterationAp == findAp:
                  logger.log(1, "Updating accesspoint {0}. RSSI {1} -> {2} ".format(ap.getSsid(), self.itemsHolder[i].getRSSI(), ap.getRSSI()))
                  self.itemsHolder[i].setRSSI(ap.getRSSI())
                  self.onListModified(self.itemsHolder)
       
    def remove(self, ap):
        logger.debug("Removing accesspoint {0}".format(ap.getSsid()))
        self.itemsHolder.remove(ap)
        self.onListModified(self.itemsHolder)

    def setKarma(self, karmaAp):
        filteredAps = filter(lambda x: x.getSsid() == karmaAp.getSsid() and x.getHash() == karmaAp.getHash(), self.itemsHolder)
        for ap in filteredAps:
           logger.info("We are setting the AccessPointStatus to CERTAIN for AP {0}".format(ap.getSsid()))
           ap.setStatus(AccessPointStatus.CERTAIN)

    def getKarmaAps(self):
        return filter(lambda x: x.getStatus() == AccessPointStatus.CERTAIN, self.itemsHolder)

    def exist(self, findAp):
        for ap in self.itemsHolder:
           if ap.getHash() == findAp.getHash() and ap.getSsid() == findAp.getSsid():
              return ap
        return None

    def startAutoRemoveTimer(self):
        logger.debug("Starting autoRemoveTimer with {0} seconds interval".format(self.autoRemoveTimerInterval))
        autoRemoveTimer = threading.Timer(self.autoRemoveTimerInterval, self.removeExpired)
        autoRemoveTimer.start()

    def removeExpired(self):
        logger.debug("Removing expired accesspoints older than {0} seconds with a list holding {1} items".format(self.autoRemoveAge, len(self.itemsHolder)))
        for ap in self.itemsHolder:
           logger.debug("Now checking if accesspoint {0} is expired".format(ap.getSsid()))
           if ap.calcAlive() > self.autoRemoveAge:
              overtime = (ap.calcAlive() - self.autoRemoveAge)
              logger.warn("Removing accesspoint {0} which we haven't seen for {1} seconds. Overtime: {2} seconds".format(ap.getSsid(), ap.calcAlive(), overtime))
              self.remove(ap)
           else:
              logger.debug("Accesspoint {0} is not expired yet. Last seen in seconds {1}".format(ap.getSsid(), ap.calcAlive()))
        self.onAutoRemoveTimerFinished()

    def getIndexByAp(self, ap):
        index = -1
        for i, iterationAp in enumerate(self.itemsHolder):
            if iterationAp.getSsid() == ap.getSsid() and iterationAp.getHash() == ap.getHash():
               index = i
               break
        return index
            

    def findBySsid(ssid):
        for ap in self.itemsHolder:
           if ap.getSsid() == ssid:
              return ap
        return None

    def findByHash(self, hash):
        for ap in self.itemsHolder:
           if ap.getHash() == hash:
              return ap
        return None

    def findSimilairBySsid(self, ap):
        for simAp in self.itemsHolder:
           if simAp.getSsid() == ap.getSsid():
              return simAp
        return None

    def findSimilairByHash(self, ap):
	listSimilair = []
        for simAp in self.itemsHolder:
           if simAp.getHash() == ap.getHash() and simAp.getSsid() != ap.getSsid():
              listSimilair.append(simAp)
        return listSimilair

    def countBySsid(ssid):
        x = 0
        for ap in self.itemsHolder:
           if ap.getSsid() == ssid:
              x += 1
        return x
 
    def getStatus(self, ap):
        # AccessPoint responded to our fake SSID will get the status CERTAIN
        if ap.getStatus() == AccessPointStatus.CERTAIN:
           similairHashAps = self.findSimilairByHash(ap)
           for iterationAp in similairHashAps:
               iterationAp.setStatus(AccessPointStatus.TENTATIVE)
         
        return ap.getStatus()           

def clean():
    # @FIXME This ugly code should be fixed.
    os.system('clear')

def renderList(list):
    table = PrettyTable(["Fingerprint", "Channel", "RSSI", "SSID", "Status"])
    table.align["Status"] = "r"
    table.align["SSID"] = "l"

    for ap in list:
        table.add_row([ap.getHash(), ap.getChannel(), ap.getRSSI(), ap.getSsid(), manager.getStatus(ap)])
    return table


def listModified(modifiedList):
    clean()
    print renderList(modifiedList)
    time.sleep(0.1)

def probeReceived(ap):
    logger.info("We received the probe that was send: " + ap.getSsid())
    os.system("sudo beep -f 1000 -l 100 -n -f 2000 -l 500 -n -l 20 -f 2500") 

def sniffer(pkt):
    if pkt.haslayer(Dot11) and pkt.haslayer(Dot11Elt):
        if pkt.subtype in (8, 5):
           ap = AccessPoint(packet=pkt)
           if manager.exist(ap) == None:
              manager.add(ap)
           else:
              manager.update(ap)

# Logger settings
handler = logging.FileHandler(logfile)
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(loglevel)

manager = AccessPointManager()
manager.setAutoRemoveTimerInterval(autoRemoveTimerInterval)
manager.setAutoRemoveAge(autoRemoveAge)
manager.onListModified += listModified

prober = Prober()
prober.setSsid(ssid)
prober.setSourceMac(sourceMac)
prober.setFreqChangeSleep(freqChangeSleep)
prober.watchManager(manager)
prober.sendProbe()

logger.debug("Starting sniffing")
sniff(iface=interface, prn=sniffer)
